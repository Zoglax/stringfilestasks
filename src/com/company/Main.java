package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
        PrintStream writer = new PrintStream("output.txt");

        //1
        //1.1
        /*
        String curentStr
        int sumStrings=0;
        while ((curentStr=reader.readLine())!=null)
        {
            sumStrings++;
        }

        writer.println(sumStrings);*/
        //1.2
        /*
        String curentStr
        int sumChars=0;
        while ((curentStr=reader.readLine())!=null)
        {
            sumChars+=curentStr.length();
        }

        writer.println(sumChars);*/

        //2
        /*String curentStr
        while ((curentStr=reader.readLine())!=null)
        {
            writer.println(curentStr.length());
        }*/

        //3
        //3.1
        /*curentStr=reader.readLine();
        writer.println(curentStr);*/

        //3.2
        String curentStr = reader.readLine();
        int countStrings = Integer.parseInt(curentStr);
        for (int i = 0; i < countStrings; i++) {
            curentStr = reader.readLine();
            if (i==5)
            {
                writer.println(curentStr);
            }
        }

        writer.close();
        reader.close();
    }
}
